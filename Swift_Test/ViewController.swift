//
//  ViewController.swift
//  Swift_Test
//
//  Created by Adrian on 27/11/15.
//  Copyright © 2015 Adrian. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, MKMapViewDelegate, CLLocationManagerDelegate {

    //Outlets
    
    @IBOutlet weak var lblLat: UILabel!
    
    @IBOutlet weak var lblLong: UILabel!
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var tblContainer: UIView!
    
    @IBOutlet weak var tblContainerVCenterConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var tblView: UITableView!
    
    //Properties
    
    var locationManager:CLLocationManager!
    
    var tmpTblData:NSMutableArray!
    
    var isShown:Bool!
    
    var userPin:MKPointAnnotation!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        locationManager = CLLocationManager()
        
        tmpTblData = ["1st Cell", "2nd Cell", "3rd Cell", "4th Cell", "5th Cell"]
        
        isShown = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        checkLocationAuthorizationStatus()
        
        startUpdateLocation()
    }

    // MARK: - Helpers
    
    
    func startUpdateLocation() {
        if(CLLocationManager.locationServicesEnabled())
        {
            locationManager.distanceFilter = kCLDistanceFilterNone
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.delegate = self
            self.locationManager.startUpdatingLocation()
        }
    }
    
    func checkLocationAuthorizationStatus() {
        
        if CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse && CLLocationManager.authorizationStatus() == .AuthorizedAlways {
            mapView.showsUserLocation = true
        } else {
            self.locationManager.requestWhenInUseAuthorization()
            self.locationManager.requestAlwaysAuthorization()
        }

    }
    
    // MARK: - CoreLocatinManager Delegate Method
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        let currLocation:CLLocation! = locations.last
        
        lblLat.text = NSString(format: "%f", currLocation.coordinate.latitude) as String
        lblLong.text = NSString(format: "%f", currLocation.coordinate.longitude) as String
        
        if self.userPin == nil {
            self.userPin = MKPointAnnotation()
        }
        
        let span:MKCoordinateSpan! = MKCoordinateSpanMake(0.01, 0.01)
        let region:MKCoordinateRegion! = MKCoordinateRegionMake(currLocation.coordinate, span)
        userPin.coordinate = currLocation.coordinate
        
        self.mapView.setRegion(region, animated: true)
        self.mapView.mapType = MKMapType.Standard
        self.mapView.addAnnotation(userPin)
    }
    
    // MARK: - MKMapViewDelegate Methods
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if (annotation is MKUserLocation) {
            return nil
        }
        
        var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier("userLocation")
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "userLocation")
            annotationView!.canShowCallout = true
        }
        annotationView?.image = UIImage(named: "pin");
        
        return annotationView
    }
    
    // MARK: - UITableView DataSouce and Delegate Methods
   
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tmpTblData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "customCell")
        cell.textLabel!.text = tmpTblData[indexPath.row] as? String
        return cell
    }
    
    // MARK: - Actions
    
    @IBAction func tapPressMe(sender: AnyObject) {
        var moveLocation:CGFloat!
        let screenHeight:CGFloat = UIScreen.mainScreen().nativeBounds.size.height
        
        if isShown == true {
            moveLocation = -screenHeight / 2 - 10
            tblContainerVCenterConstraint.constant = 0
        }
        else
        {
            tblContainer.hidden = false
            tblContainerVCenterConstraint.constant = -screenHeight / 2 - 10
            moveLocation = 0
        }
        
        self.view.layoutIfNeeded()
        
        UIView.animateWithDuration(0.8, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options:UIViewAnimationOptions.AllowUserInteraction, animations: { () -> Void in
            self.tblContainerVCenterConstraint.constant = moveLocation
            self.view.layoutIfNeeded()
        }) { (Bool) -> Void in
            self.isShown = !self.isShown
        }
    }
    
}